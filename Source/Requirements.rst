Requirements
##############

Before you begin, be aware that you **may need**

  - **Public access** to your instance, wich in some cases requires a public IP, a DNS pointer, or a VPN connection for your Network. 

  - The remote access to your instance is made via SSH and **requires port 22 to be open** [*]_.

  - The `Hardware`_ requirements may vary depending on scaling factors and must be selected according to your particular scenario. The table shown below is just a **recomendation** and it is **not mandatory** to follow its content.
  - The `OS`_, `Specific cloud plattform configurations`_ and `Networking`_ requirements are **mandatory**. The missheeding of the following requirements may lead to failure on installation process.

*In doubt, contact your IT department to find the best configuration depending on your scenario.*

.. [*] Networking_ subsection contains a complete list of ports needed to be open and Appendix-D_ contains instructions to verify if you have the right tools to work with SSH

OS
**********

    - Ubuntu Server 18.04.X LTS (Bionic Beaver)
    - *ubuntu* user member of *sudoers* group.

HARDWARE
*********

The following list of requirements varies depending on each case, and would be defined by the porpouse of your *Basic Kernel instance*, the size of your *LoRaWAN network*, the number of devices connecting to your server, etc.

The following guidelines may help you to select hardware.

For evaluation purposes:
========================

+---------+---------------------+---------------------+
| Feature |      Min            |    Recomended       |
+=========+=====================+=====================+
|Processor|            1  CPU or vCPU @3.3GHz         |
+---------+---------------------+---------------------+
| Memory  |        1 GB         |        2  GB        |
+---------+---------------------+---------------------+
|   Disk  |        8 GB         |        20 GB        |
+---------+---------------------+---------------------+
| Network |        An Internet Stable conection       |
+---------+---------------------+---------------------+



For production workload purposes:
=================================

+---------+--------------------------+--------------------------+
| Feature |          Min             |      Recomended          |
+=========+==========================+==========================+
|Processor|                 2  CPU or vCPU @3.1GHz              |
+---------+--------------------------+--------------------------+
| Memory  |          8  GB           |          32 GB           |
+---------+--------------------------+--------------------------+
|   Disk  |          20 GB           |    Greater than 128 GB   |
+---------+--------------------------+--------------------------+
| Network | 3,500 Mbps Dedicated Bandwidth with Internet access |
+---------+--------------------------+--------------------------+

Networking
**********

The following ports **must** be open: 

WAN/Internet/Outbound

+--------+--------------+-----------------------+-------------+
|Protocol|  Port Range  |         Use           | Description |
+========+==============+=======================+=============+
|   TCP  | 7000 - 8000  | Basic Kernel services |             |
+--------+--------------+-----------------------+-------------+
|   UDP  | 7000 - 8000  |        https          |             |
+--------+--------------+-----------------------+-------------+
|   TCP  |     443      |        https          |             |
+--------+--------------+-----------------------+-------------+
|   TCP  |     80       |        http           |             |
+--------+--------------+-----------------------+-------------+
|   TCP  |     22       |        SSH            |             |
+--------+--------------+-----------------------+-------------+
|   TCP  |     25       |        SMTP           |             |
+--------+--------------+-----------------------+-------------+

LAN/VPC/Inbound                                             

+--------+--------------+-----------------------+-------------+
|Protocol|  Port Range  |         Use           | Description |
+========+==============+=======================+=============+
|   TCP  |     5432     |     PostgreSQL DB     |             |
+--------+--------------+-----------------------+-------------+
|   UDP  |      443     |        https          |             |
+--------+--------------+-----------------------+-------------+
|   TCP  |      80      |        http           |             |
+--------+--------------+-----------------------+-------------+

Specific cloud plattform configurations
******************************************

**Before proceding** be aware that you need to get access via SSH protocol, for what you will need an *ssh client*, as well as some *ssh tools* wich comes installed by default on most Linux environments. On Windows, a set of those comes installed by default since *Windows build 16215*. We strongly recommend first take a look to `Appendix D: Check for present SSH on OS`_ were are instructions to check if this tools are present on your system as well as a magic link that brings you back to continue just were you left.

Now that we are sure your system is ready, you may have a *remote access* procedure defined. If this is your case, feel free to select your plattform from the list bellow. If you don't `Appendix F: Configure remote access`_ have instructions to achieve this endevour.  

    - Azure_

    - AWS_

    - GCP_

..    - Python 3.5 or greater
..    - pip 19.1.1 

.. _AWS: Appendix-A.html
.. _GCP: Appendix-B.html
.. _Azure: Appendix-C.html
.. _Appendix-D: Appendix-D.html
.. _`Appendix F: Configure remote access`: Appendix-F.html
.. _`Appendix D: Check for present SSH on OS`: Appendix-D.html