Appendix F: Configure remote access
####################################

1. Open a command Window

    1.1 Press *win* and *r* keys symultanously 

    .. figure:: RunWindow.PNG
        :width: 75%

    1.2 Type *cmd* and hit *enter* to open a *command window*

    .. figure:: RunAndCMDWindow.PNG
        :width: 75%


    .. figure:: cmdWindow.png
        :width: 75%

    
2. Check for the *.ssh* folder

+-----------------------------+-----------------------------+
|  On linux                   | On windows                  |
+=============================+=============================+
|                             |                             |
|  .. code-block:: sh         |  .. code-block:: powershell |
|     :caption: use ls        |     :caption: use dir       |
|                             |                             |
|     $ls  ~/.ssh             |      >dir %HOMEPATH%\.ssh   |
|                             |                             |
+-----------------------------+-----------------------------+

    **Just in case you have received an error like**: 

        - *File Not Found* (windows) 
        - *ls: cannot access '/home/user/.ssh': No such file or directory* (linux) 
 
    run :
        +-----------------------------+-----------------------------+
        |  On linux                   | On windows                  |
        +=============================+=============================+
        |                             |                             |
        |  .. code-block:: sh         |  .. code-block:: powershell |
        |     :caption: create folder |     :caption: create folder |
        |                             |                             |
        |     $mkdir ~/.ssh           |      >mkdir %HOMEPATH%\.ssh |
        |                             |                             |
        +-----------------------------+-----------------------------+


3. Change directory

        +-----------------------------+-----------------------------+
        |  On linux                   | On windows                  |
        +=============================+=============================+
        |                             |                             |
        |  .. code-block:: sh         |  .. code-block:: powershell |
        |     :caption: move to .ssh  |     :caption: move to .ssh  |
        |                             |                             |
        |     $cd ~/.ssh              |      >cd    %HOMEPATH%\.ssh |
        |                             |                             |
        +-----------------------------+-----------------------------+


4. Generate ssh keys

This command is the same in both windows [*]_ and linux systems: 

.. code-block:: sh
      :caption: Generate ssh key pair 
      
      ssh-keygen -f aws_key -t rsa -b 4096  -C ubuntu -N ""

.. [*] Assuming you have followed Apendix-D and/or Appendix-E instructions.

5. NEVER SHARE YOUR PRIVATE KEY 

Remember that now you have created a pair of keys that allows 

You may be interested on continue with the `Specific plattform configurations`_

.. _`Specific plattform configurations`: Requirements.html#specific-plattform-configurations