Basic Kernel Overview
=====================

**Basic Kernel** is a *LoRaWAN Network Server* (*LNS*) that connects *LoRaWAN-compliant* devices to application backends in a star of stars topology. It is the first of a new generation of *LoRaWAN Network Servers* specifically designed along the following guiding principles:

    - Carrier-grade robustness and security in a true multi-tenancy architecture.
    
    - Multi-dimensional scale-out both for devices and gateways to facilitate large-scale (country-wide) traditional tower-based deployments as well as hybrid deployments following the inside-out deployment model.
    
    - High degree of deployment flexibility in both dedicated and hosted environments (*LoRa Network Server as a Service, LNSaaS*) facilitating a broad range of customer requirements.

Whithin the *Basic Kernel* ecosystem, the *LoRaWAN Gateways* are called *Basic Stations* and those are connected to *Basic Kernel* via standard, secured IP connections while devices use single-hop *LoRaWAN communication* to one or many *Basic Stations*. *Basic Kernel*, in turn, relays messages between *Basic Stations* and any number of application backends. It further provides a network operator with all the network management functionality required for large-scale (country-wide) deployments.

Core Components
----------------

*Basic Kernel* consists of the following core components: 

    `Basic Station`_: On-gateway packet forwarder.

    `Basic Kernel Dispatch`_: TrackCentral Dispatch LNS core.
    
    `Basic Kernel IO`_: application router.
    
    `Basic Kernel Terminal`_: management console.

.. figure:: 1-1CoreComponents.png
    :width: 100%

Fig. 1: TrackCentral Architecture.

Basic Station
^^^^^^^^^^^^^

**Basic Station** is the  *on-gateway* packet forwarder software, which:

    - Bi-directionally relays traffic between devices and Dispatch, adding operational meta-data such as timestamps, received signal-strength indicators (RSSI), and signal-to-noise ratios (SNR).
    
    - Protects the backhaul link between *Basic Stations* and *Basic Kernel Dispatch* as well as *Basic Kernel Dispatch* itself from malicious traffic. 

*Basic Stations* can be remotely managed including firmware upgrades via *Basic Kernel Terminal*.

Basic Kernel Dispatch
^^^^^^^^^^^^^^^^^^^^^

**Basic Kernel Dispatch** bundles all the core *LoRaWAN* network management functionality, allowing for multi-dimensional scale-out both at the device and gateway level. As such it:

    - Ensures the integrity of messages received from devices, relaying only those messages that are cryptographically verified as genuine to the respective *Basic Kernel IO* instances serving as proxies for device-owning application backends.

    - Schedules all downlink communication initiated either from within *Basic Kernel* (*Dispatch, IO, or Terminal*) itself, or from application backends in accordance with the regional regulations imposed on transmitting downlink messages.

    - Optimizes device connectivity per device and across the overall device population by performing dynamic data-rate adaptation for devices mounted at fixed locations or nomadic devices during periods of rest.

    - Manages the security and compliance of the infrastructure and devices deployed by performing regulatory sanity checks, blacklisting misbehaved devices, and issuing new cryptographic keys on demand in cooperation with the respective *join servers*.

    - Collects and (pre-)aggregates usage data for network operation and billing.

Although shown as a single component, *Basic kernel dispatch* internally is built from a larger number of sub-components for horizontal scale out on demand.

Basic Kernel IO
^^^^^^^^^^^^^^^

**Basic Kernel IO instances** are representatives of application backends interfacing with *Basic kernel Dispatch* for connectivity, typically (but not necessarily) under the control of the application owner. Conceptually there is one *Basic Kernel IO* instance per application backend that:

    - Relays traffic between *Basic Kernel Dispatch* and the customer application backend.

    - Performs all application-level cryptographic encryption / decryption operations on behalf of the application backend.

Separating the *Basic Kernel IO* functionality from *Basic Kernel Dispatch* is key for the true multi-tenancy infrastructure of *Basic Kernel*. In contrast to integrated solutions, with *Basic Kernel* the network operator is never given access to device-specific application keys and decrypted application payloads unless explicitly authorized by the application owner. Limiting access following a strict need-to-know model protects both the network operator and the application owner likewise.

Basic Kernel Terminal
^^^^^^^^^^^^^^^^^^^^^

**Basic Kernel Terminal** is the management and administration component that:

    - Provides maintenance and operational interfaces for network operators.

    - Provides operational statistics and deep-inspection tools for network management and optimization across all components.

    - Allows device and gateway management including remote gateway configuration and firmware updates for network operators and application owners.

Basic Kernel Services
---------------------

Besides the aforementioned core components, *Basic Kernel* further comes with a couple of support services such as:

    - **Basic Kernel Join Server**: Implements the *join-server* functionality of *LoRaWAN 1.1* and authorizes new devices towards different *LNS* instances (including *Basic Kernel*) in response to over-the-air activation requests.

    - **Basic Kernel Usher**: Distributes initial configurations and security credentials for specific *Basic Kernel* instances to connecting *Basic Stations*.

    - **Basic Kernel Maps**: Provides coverage estimates for hybrid *LoRaWAN* deployments of both indoor and outdoor *Basic Stations*.
