Appendix E: Activate WLS
############################

WSL (Windows Subsystems for linux) is a set of tools which allows to run some *linux features* from *windows command window* (more details at: `Windows Subsystems for linux`_). The  list of **features required** to be installed to follow this guide is:

  - ssh client
  - scp
  - ssh-keygen
  - curl

Please follow this steps in order to activate this features: 

**CAUTION: reboot required, save all your work before proceeding**

1. Open a powershell Window as administrator or with elevated privileges.

2. Run the following command:

   .. code-block:: powershell
      :caption: Enabling WSL

      >Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux

3. Perform a reboot [*]_

   .. code-block:: powershell
      :caption: Reboot system
      
      >shutdown -r -t 0

Once rebooted, you can continue with installation_

.. _installation: Installation.html
.. _`Windows Subsystems for linux`: https://docs.microsoft.com/en-us/windows/wsl/install-win10
.. [*] This is required to load new features. 