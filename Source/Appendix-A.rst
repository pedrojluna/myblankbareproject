Appendix A: AWS Configs
############################

1) Log into your console:

.. figure:: AWSlogin.png
    :width: 75%

2) Go to *Services>EC2*

.. figure:: Sercices-EC2.png
    :width: 75%

3) Create a Security Group

    3.1) select *security groups*

    .. figure:: SecurityGroups.PNG
        :width: 100%

    3.2) Click on *Create a Security Group*

    .. figure:: createSG.PNG
        :width: 100%

    3.3) Select a descriptive name for the group (optionally), add a description and click on *add rule*

    .. figure:: addRlz.PNG
        :width: 100%

    3.4) Set the parameters for the security group.
    
    - Select the *Inbound tab* and add the  following protocol rules:
        
        - *SSH* 
            - *Source*: Anywhere

        - *HTTP*
            - *Source*: Anywhere

        - *HTTPS* 
            - *Source*: Anywhere

        - *Custom TCP Rule* 
        
            - *Port Range*: 7000-8000
            - *Source*: Anywhere
            - *Description*: Basic Kernel Ports
        
        - *Custom UDP Rule*
    
            - *Port Range*: 7000-8000
            - *Source*: Anywhere
            - *Description*: Basic Kernel Ports

    - Leave the Outbound tab with its default settings.
    - Click *Create* to finish creation.

    .. figure:: FinishSG.PNG
        :width: 100%

4) Upload your keypair

    4.1) Select *Key Pairs* option from the side menu on *EC2* section.
    
    .. figure:: SelectKeyPairsmenu.PNG
        :width: 100%
    
    4.2) Click on *Import Key Pairs* button.
    
    .. figure:: ImporKeyPair.PNG
        :width: 100%

    4.3) You can either:

+---------------------------------------------+---------------------------------------------+
|Upload Key pair from your PC                :|Copy and paste your **PUBLIC KEY**          :|
+---------------------------------------------+---------------------------------------------+
|.. figure:: LaunchInstanceA.png              |.. figure:: LaunchInstanceB.png              |
|   :width: 100%                              |   :width: 100%                              |
+---------------------------------------------+---------------------------------------------+

    click on *Import* to finish

    .. figure:: ImporKeyPair.PNG
        :width: 100%


5) Create an instance

  5.1) Click on the *Launch Instance* button.

+---------------------------------------------+---------------------------------------------+
|From *EC2 Dashboard*                        :|From *Instances* section                    :|
+---------------------------------------------+---------------------------------------------+
|.. figure:: LaunchInstanceA.png              |.. figure:: LaunchInstanceB.png              |
|   :width: 100%                              |   :width: 100%                              |
+---------------------------------------------+---------------------------------------------+

    5.2) Search and select *Ubuntu Server 18.04 LTS (HVM), SSD Volume Type* (or the closest option) 

    .. figure:: ubuntu184.png
        :width: 100%

    5.3) Choose your instance type according to your needs and then click on the *Next: Configure Instance Details* button. 

    .. figure:: ChooseInstanceType.png
        :width: 50%
    
    5.4) Althought you need some special configuration you can use the default values from *Configure Instance Details* menu, click on *Next: Add Storage* to continue.

    .. figure:: InstanceDetails.png
        :width: 50%

    5.5) Select the size and number of your disks depending on your particular needs and click on *Next: Add Tags* to continue.

    .. figure:: AddStorage.png
        :width: 50%

    5.6) You can optionally set your instance machine to ease identification by adding the *Name* tag as shown in example. **Note** the capital letter, since tags are case sensitive. click on *Next: Configure Security Group*

    .. figure:: AddTags.png
        :width: 100%
    
    5.7) Chooose the *select an existing security group* option, as well as the *Basic Kernel Security Group* we have created on step 3. click on *Review and Launch* to continue.

    .. figure:: AddSecurityGroup.png
        :width: 100%

    5.8) Review the details and click on *Launch* to finish instance creation.

    .. figure:: finishInstanceCreation.png
        :width: 50%

    5.9) Select the keypar uploaded on step 4. 

    .. figure:: finishInstanceCreation.png
        :width: 100%