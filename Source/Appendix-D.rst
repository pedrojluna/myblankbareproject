Appendix D: Check for present SSH on OS
#######################################

1. Open a *Run command* Window by symultanously pressing *win* and *r* keys

.. figure:: RunWindow.png
    :width: 75%

    Fig. 1: Run window.

2. Type *cmd* and hit *enter* to open a *command window*

.. figure:: RunAndCMDWindow.png
    :width: 75%

    Fig. 2:Open a command window 

.. figure:: cmdWindow.png
    :width: 75%

    Fig. 3:Command window 

3. Type *ssh* and hit *enter*.

.. figure:: entershh.png
    :width: 75%

    Fig. 4: entering ssh command.

4. Check the optput

Check your screen for one of the following outputs

If you have...

+---------------------------------------------+---------------------------------------------+
| This one, then you are good to go.          | This one, then you need to install ssh.     |
+---------------------------------------------+---------------------------------------------+
|.. figure:: sshInstalled.png                 | .. figure:: sshInstalled.png                |
|   :width: 100%                              |    :width: 100%                             |
|                                             |                                             |
|   Fig. 4: SSH already installed.            |    Fig. 4: SSH not installed.               |
|                                             |                                             |
|   `click here to continue`_                 |    `click here for further instructions`_   |
|                                             |                                             |
+---------------------------------------------+---------------------------------------------+


.. _`click here to continue`: Requirements.html#specific-plattform-configurations
.. _`click here for further instructions`: Appendix-E.html