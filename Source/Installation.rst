Installation
############

**Before start** : In order to get the most out of this guide, it is recommended to first check [*]_ : 
   - You have read and fullfill the `requirements`_ section. 
   - The *ubuntu* user exist on your target server and it is a member of the sudoers group.
   - You have *SSH* or *on-site* access to the server you want to configure.

.. [*] **this guide asumes that you are using this settings** and the misheeding of this indication could lead to failure on installation process.

1 . Copy the tarball to your server / log in 
*********************************************

Depending on your case you probably are on one of the following scenarios: 

+-------------------------------------------------------------------+------------------------------------+
| You are installing *basic kernel* on:                             | Will perform installation:         |
+===================================================================+====================================+
|- A computer with usb ports disabled/inaccesible                   | From a remote `windows`_ machine   |
|- VMware, VirtualBox, Azure, AWS, GCP or any other virtual machine.+------------------------------------+
|                                                                   | From a remote `linux`_ machine     |
+-------------------------------------------------------------------+------------------------------------+
|- A computer with usb ports enabled and accesible                  | From the same physical_ machine    |
+-------------------------------------------------------------------+------------------------------------+


.. _windows:

1.A Installing from a Windows machine
======================================

+----------------------------------------------------------------------------------------------+
| 1.A                                                                                          |
+==============================================================================================+
|                                                                                              |
|-Download: *tc_TrackCentral_1.2.7-156-gab73feff.tgz* file                                     |
| to your default *Downloads* folder (*V.G. C:\\Users\\myuser\\Downloads*)                     |
|                                                                                              |
|- Upload the file to your target server                                                       |
|                                                                                              |
|.. code-block:: bat                                                                           |
|   :caption: Uploading from a windows machine                                                 |
|                                                                                              |
|   >scp %HOMEPATH%\Downloads\tc_TrackCentral_1.2.7-156-gab73feff.tgz ubuntu@<your.host.com>:~/|
|                                                                                              |
|- Log remotelly into your server                                                              |
|                                                                                              |
|.. code-block:: bat                                                                           |
|   :caption: login                                                                            |
|                                                                                              |
|   >ssh ubuntu@<your.host.com>                                                                |
|                                                                                              |
|                                                                                              |
|                                                                                              |
+----------------------------------------------------------------------------------------------+


.. _linux :

1.B Installing from a linux machine
======================================

+----------------------------------------------------------------------------------------------+
|  2.B                                                                                         |
+==============================================================================================+
|-Download: *tc_TrackCentral_1.2.7-156-gab73feff.tgz* file                                     |
|to your default *Downloads* folder (*V.G. /home/myuser/Downloads*)                            |
|                                                                                              |
|- Upload the file to your target server                                                       |
|                                                                                              |
|.. code-block:: sh                                                                            |
|   :caption: Upload the file to the server                                                    |
|                                                                                              |
|   $scp ~/Downloads/tc_TrackCentral_1.2.7-156-gab73feff.tgz ubuntu@<your.host.com>:~/         |
|                                                                                              |
|- Log remotelly into your server                                                              |
|                                                                                              |
|.. code-block:: sh                                                                            |
|   :caption: login                                                                            |
|                                                                                              |
|   >ssh -A ubuntu@<your.host.com>                                                             |
|                                                                                              |
|                                                                                              |
|                                                                                              |
+----------------------------------------------------------------------------------------------+

.. _physical:

1.C installing directly on the server
======================================
+----------------------------------------------------------------------------------------------+
|  1.C [*]_                                                                                    |
+==============================================================================================+
|                                                                                              |
|  .. code-block:: sh                                                                          |
|     :caption: login and copy file                                                            |
|                                                                                              |
|     login: ubuntu                                                                            |
|     password:                                                                                |
|     $mount /dev/sdb1 /mnt                                                                    |
|     $cp /mnt/tc_TrackCentral_1.2.7-156-gab73feff.tgz. ~                                      |
|                                                                                              |
+----------------------------------------------------------------------------------------------+

.. [*] Please note we are assumming **(a)** You are using a thumbdrive and system has labeled it as *sdb1*, **(b)** Automount is disabled and **(c)** the *tc_TrackCentral_1.2.7-156-gab73feff.tgz* file is on the root of your Thumbdrive.

3. Extract the tarball
***********************

.. code-block:: console
   :caption: Tarball Extraction
   
   $tar -xvzf tc_TrackCentral_1.2.7-156-gab73feff.tgz

Clean unecesary files

.. code-block:: console
   :caption: removing tar 
   
   $rm tc_TrackCentral_1.2.7-156-gab73feff.tgz

4. Run prerequisite scripts
****************************

.. code-block:: console
   :caption: Install prerequistes 
   
   ~/trackcentral/recipes/ubuntu-18.04/setup-tcbase.sh

.. code-block:: console
   :caption: Configure the Database
   
   ~/trackcentral/recipes/ubuntu-18.04/setup-pglocal.sh

5. Install Basic Kernel
************************

   +--------------------------------------------------------------------------------------------------+
   |                     Install using a FQDN  (DNS service available)                                |
   +--------------------------------------------------------------------------------------------------+
   |                                                                                                  |
   |  .. code-block:: sh                                                                              |
   |     :caption: Install Kernel                                                                     |
   |                                                                                                  |
   |     ~/trackcentral/recipes/ubuntu-18.04/tc-install/install tc hostname=<host>                    |
   |                                                                                                  |
   |     ~/trackcentral/recipes/ubuntu-18.04/tc-install/install tc hostname=kernel.example.com        |
   |                                                                                                  |
   +--------------------------------------------------------------------------------------------------+
   |                     Install using public IP address (DNS service unavailable)                    |
   +--------------------------------------------------------------------------------------------------+
   |                                                                                                  |
   |  .. code-block:: sh                                                                              |
   |     :caption: Install Kernel                                                                     |
   |                                                                                                  |
   |     ~/trackcentral/recipes/ubuntu-18.04/tc-install/install tc hostname=<ip>                      |
   |                                                                                                  |
   |     ~/trackcentral/recipes/ubuntu-18.04/tc-install/install tc hostname=1.1.1.1                   |
   |                                                                                                  |
   +--------------------------------------------------------------------------------------------------+
   



.. _requirements: Requirements.html