Installation Guide for basic kernel 
####################################

Welcome to basic kernel!
*************************

In this guide you will find instructions to get started with Basic Kernel. 

It could contain basic information you already know, if it's the case, you may be interested on `How to read this guide`_ and skip some sections. Otherwise, you should start reading the with the `Overview`_ section and continue reading all sections consecutively.

How to read this guide
***********************

This guide is divided on sections as follows: 

+----------------------+--------------------------------------------------------------------------------------------+
|                      |                                                                                            |
|                      |Contains details about Basic Kernel architecture and its relation with LoRaWAN              |
|`Overview`_           |specifiction.If you have previous knowledge of Basic Kernel or don't feel the               |
|                      |need to get on details about this topic you may skip this section.                          |
|                      |                                                                                            |
+----------------------+--------------------------------------------------------------------------------------------+
|                      |                                                                                            |
|`Get-Basic-Kernel`_   |If you have the files needed for installation skip this section.                            |
|                      |                                                                                            |
+----------------------+--------------------------------------------------------------------------------------------+
|                      |                                                                                            |
|`Requirements`_       |Consider this as a check list to properly install Basic Kernel on your Server.              |
|                      |                                                                                            |
+----------------------+--------------------------------------------------------------------------------------------+
|                      |                                                                                            |
|`Installation`_       |Step by step guide to install basic kernel in your instance.                                |
|                      |                                                                                            |
+----------------------+--------------------------------------------------------------------------------------------+
|                      |                                                                                            |
|`Configuration`_      |Post installation configurations.                                                           |
|                      |                                                                                            |
+----------------------+--------------------------------------------------------------------------------------------+

Index
###############################

.. toctree::
    :maxdepth: 4
    
    Overview
    Get-Basic-Kernel
    Requirements
    Installation
    Configuration
    Appendix-A
    Appendix-B
    Appendix-C
    Appendix-D
    Appendix-E
    Appendix-F

.. _Overview: Overview.html
.. _Requirements: Requirements.html
.. _Get-Basic-Kernel: Get-Basic-Kernel.html
.. _Installation: Installation.html
.. _Configuration: Configuration.html

© Copyright 2019, Semtech Corp 