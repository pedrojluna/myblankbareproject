# Basic-documentation

## Before you start

Sphinx tool is used to crete this set of documentation.

>_*NOTE: if running on windows you may need to first install [MiKTeX](https://miktex.org/download) and [perl](https://www.activestate.com/products/activeperl/downloads/)_

- It is mandatory to have installed pyton 3.5 or greater.
- Pip is also required.
- It is not necessary but desirable to use Virtual Environments.

Install python dependencies with _*_:

  `pip install -r requirements.txt`

>_NOTE: This should be run under the project's main folder since the *requirements.txt* file is located there_